const promise = require('bluebird');
const options = {
   // Initialization Options
   promiseLib: promise
};

const pgp = require('pg-promise')(options);
const connectionString = 'postgres://postgres:1234@localhost:5432/OfflineDB';
const db = pgp(connectionString);
console.log('==> const db = pgp(connectionString);');

exports.db = db;
