var express = require('express');
var router = express.Router();

const restTicket = require('../rest/ticket');
const restActivity = require('../rest/activity');


router.get('/api/synchroActivities/:ticketPar/:lastUpdatePar', restActivity.synchroActivities);

router.get('/api/getTicket/:userPar/:domainPar/:passPar', restTicket.getTicket);

router.get('/api/getActivities', restActivity.readActivities);

router.get('/', (req, resp) => {
    resp.send('<html>');
    // resp.send('Pruebe ... <a href="http://localhost:3000/api/getActivities">getActivities</a>');
    resp.send("Probando...");
    resp.end('</html>');
});

// router.post('api/activity', restActivity.createActivity);
// router.put('api/activity/:id', restActivity.updateActivity);
// router.delete('api/activity/:id', restActivity.removeActivity);

module.exports = router;
