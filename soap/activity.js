const wsdl = require('./wsdl');
let dataWS = null;
function getDataActivities() { return dataWS; };

const getActivities = (req, resp, ticket, callback) => {
   const methodParams = {
      ticket: ticket //,
      //fechaCorte: decodeURIComponent(req.params.lastUpdatePar),
   };

   console.log('==> masterTablesClient.ObtenerActividades');

   wsdl.masterTablesClient().ObtenerActividades( methodParams, function (err, result) {
      if(err) {
         console.log('==> ERROR: ', err);
         return callback(err);
      }

      console.log('==> ObtenerActividades');
      dataWS = result.ObtenerActividadesResult.Actividad;
      return callback();

   })
};

exports.getActivities = getActivities;
exports.getDataActivities = getDataActivities;