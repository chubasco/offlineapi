const soap = require("soap");

const soapAuthenticationURL = 'http://190.151.14.104/Saydex.ServidorSiap.WebService/Autentificacion.asmx?wsdl';
let authenticationClient = null;
let authenticationClientLastUpdate = new Date();
function getAuthenticationClient() { return authenticationClient; };

const soapMasterTablesURL = 'http://190.151.88.204/systmg/10/site/estructurabajada.asmx?wsdl';
let masterTablesClient = null;
let masterTablesClientLastUpdate = new Date();
function getMasterTablesClient() { return masterTablesClient; };

const refreshMasterTablesWSDL = (callback) => {
   console.log('==> masterTablesClient');
   if(masterTablesClient != null && ((new Date())- masterTablesClientLastUpdate)<100000) {
      return callback();
   }

   console.log('==> soap.createClient');
   soap.createClient(soapMasterTablesURL, (err, client) => {
      if (err) {
         console.log('==> ERROR: ', err);
         return callback(err);
      }

      client.addHttpHeader('Rayen-Client-Version','11.1.1');
      masterTablesClient = client;
      masterTablesClientLastUpdate = new Date();
      return callback();
   });
};

const refreshAuthenticationWSDL = (callback) => {
   console.log('==> authenticationClient');
   if(authenticationClient != null && ((new Date())- authenticationClientLastUpdate)<100000) {
      return callback();
   }

   console.log('==> soap.createClient');
   soap.createClient(soapAuthenticationURL, (err, client) => {
      if (err) {
         console.log('==> ERROR: ', err);
         return callback(err);
      }

      client.addHttpHeader('Rayen-Client-Version','11.1.1');
      authenticationClient = client;
      authenticationClientLastUpdate = new Date();
      return callback();
   });
};

exports.authenticationClient = getAuthenticationClient;
exports.masterTablesClient = getMasterTablesClient;
exports.refreshMasterTablesWSDL = refreshMasterTablesWSDL;
exports.refreshAuthenticationWSDL = refreshAuthenticationWSDL;

