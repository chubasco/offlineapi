const async = require('async');
const wsdl = require('../soap/wsdl');
const soapActivity = require('../soap/activity');
const dbActivity = require('../data/activity');


const synchroActivities = (req, resp, next) => {

   console.log('==> updateActividades');

   async.series([
      wsdl.refreshMasterTablesWSDL,

      (callback) => {
         soapActivity.getActivities(req, resp, decodeURIComponent(req.params.ticketPar), callback)
      },

      (callback) => {

         const activities = soapActivity.getDataActivities();
         dbActivity.synchroActivities(activities, callback);

      }
   ],
      (err) => {
         if (err) return next(err);

         resp.status(200)
            .json({
               status: 'success',
               data: soapActivity.getDataActivities(),
               message: 'All Activities Successfully Retrieved and Updated'
            });
         resp.end();
      }
   );
};

const readActivities = (req, resp, next) => {
   async.series([
         (callback) => {
            dbActivity.readActivities(callback);
         }
      ],
      (err) => {
         if (err) return next(err);

         resp.status(200)
            .json({
               status: 'success',
               data: dbActivity.getDataActivities(),
               message: 'All Activities Successfully Retrieved and Updated'
            });
         resp.end();
      }
   );
   
}

exports.synchroActivities = synchroActivities;
exports.readActivities = readActivities;