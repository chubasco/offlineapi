const wsdl = require('./wsdl');
let dataWS = null;
function getDataTicket() { return dataWS; };

const getTicket = (req, resp, login, domain, password, callback) => {

   const methodParams = {
      usuario: login, //decodeURIComponent(req.params.userPar), //'153787743',
      dominio: domain,//decodeURIComponent(req.params.domainPar), //'CesfamSaydex',
      password: password //decodeURIComponent(req.params.passPar) //'123456'
   };

   console.log('==> let info = client.ObtenerTicketDeAutentificacion');

   let info = wsdl.authenticationClient().ObtenerTicketDeAutentificacion( methodParams, function (err, result) {
      if(err) {
         console.log('==> ERROR: ', err);
         return callback(err);
      }

      console.log('==> ObtenerTicketDeAutentificacion: ', result);

      dataWS = { ticket: encodeURIComponent(result.ObtenerTicketDeAutentificacionResult)};
      return callback();

   })
   
}

exports.getTicket = getTicket;
exports.getDataTicket = getDataTicket;