const dbPg = require('./dbPg');
const db = dbPg.db;

let synchroActivitiesResult = null;

// insert/update all activities changed (received)
const synchroActivities = (activities, callback) => {
   console.log('insert/update all activities changed (received)');
   for(let activity of activities) {

      db.func('UPD_ACTIVIDAD',[activity.Id, activity.Descripcion, activity.EsOdontologico, activity.Eliminado] )

         .then (function(data) {
            console.log('UPD_ACTIVIDAD: "' + activity.Descripcion + '"  RESULT:', data[0].upd_actividad);
            activity.updated = true;
         })

         .catch(function(err) {
            console.log("ERROR:", err.message || err);
            activity.updated = false;
            synchroActivitiesResult = err;
            return callback(err);
         });

   };
   synchroActivitiesResult = 'Activities successfully synchronized'
   return callback();
}

let readActivitiesResult = null;
function getDataActivities() { return readActivitiesResult; };
// retrieve all activities
const readActivities = (callback) => {
   console.log('retrieve all activities');

   db.func('READ_ACTIVIDADES')

      .then (function(data) {
         readActivitiesResult = data;
         //console.log('UPD_ACTIVIDAD: "' + activity.Descripcion + '"  RESULT:', data[0].upd_actividad);
         return callback();
      })

      .catch(function(err) {
         console.log("ERROR:", err.message || err);
         readActivitiesResult = err;
         return callback(err);
      });
}

exports.getDataActivities = getDataActivities;
exports.readActivities = readActivities;

exports.synchroActivities = synchroActivities;
exports.synchroActivitiesResult = synchroActivitiesResult;
