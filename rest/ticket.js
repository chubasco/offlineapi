const async = require('async');
const wsdl = require('../soap/wsdl');
const soapTicket = require('../soap/ticket');

const getTicket = (req, resp, next) => {

   async.series([
         wsdl.refreshAuthenticationWSDL,

         (callback) => {

            soapTicket.getTicket(req, resp,
               decodeURIComponent(req.params.userPar),
               decodeURIComponent(req.params.domainPar),
               decodeURIComponent(req.params.passPar),
               callback
            );
         }
      ],
      (err) => {
         console.log('==> Resultado');
         if (err) return next(err);

         console.log('==> resp.status(200)');
         resp.status(200)
            .json(soapTicket.getDataTicket());
         resp.end();
         console.log('------------------------------------------');
      }
   );
}

exports.getTicket = getTicket;