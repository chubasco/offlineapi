﻿-- Table: public."ACT_ACTIVIDAD"

-- DROP TABLE public."ACT_ACTIVIDAD";

CREATE TABLE public.ACT_ACTIVIDAD
(
  ID bigserial NOT NULL,
  SERVER_ID bigint,
  DESCRIPCION character varying(5000),
  ES_ODONTOLOGICO boolean,
  ELIMINADO boolean,
  CONSTRAINT PK_ACT PRIMARY KEY (ID)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.ACT_ACTIVIDAD
  OWNER TO postgres;


-- Function: public.upd_actividad(bigint, character varying, boolean, boolean)

-- DROP FUNCTION public.upd_actividad(bigint, character varying, boolean, boolean);

CREATE OR REPLACE FUNCTION public.upd_actividad(
    parserver_id bigint,
    pardescripcion character varying,
    pares_odontologico boolean,
    pareliminado boolean)
  RETURNS boolean AS
$BODY$
DECLARE
begin
	UPDATE ACT_ACTIVIDAD SET SERVER_ID = parSERVER_ID, 
	DESCRIPCION = parDESCRIPCION,
	ES_ODONTOLOGICO = parES_ODONTOLOGICO,
	ELIMINADO = parELIMINADO
	where SERVER_ID = parSERVER_ID;

	IF NOT FOUND THEN 
	  INSERT INTO ACT_ACTIVIDAD (SERVER_ID, DESCRIPCION, ES_ODONTOLOGICO, ELIMINADO)
	  VALUES (parSERVER_ID, parDESCRIPCION, parES_ODONTOLOGICO, parELIMINADO);
	  RETURN false;
	ELSE 
	  RETURN true;  
	END IF;  
end;		
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.upd_actividad(bigint, character varying, boolean, boolean)
  OWNER TO postgres;



--DROP FUNCTION public.read_actividades();

CREATE OR REPLACE FUNCTION public.read_actividades()
RETURNS TABLE( "IdLocal" bigint, "Id" bigint, "Descripcion" varchar, "EsOdontologico" boolean, "Eliminado" boolean) AS
$BODY$
BEGIN
   RETURN QUERY select ID, SERVER_ID, DESCRIPCION, ES_ODONTOLOGICO, ELIMINADO 
   from ACT_ACTIVIDAD;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.read_actividades()
  OWNER TO postgres;